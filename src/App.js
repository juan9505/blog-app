import React from 'react';
import { Provider } from 'react-redux';
import { createStore, applyMiddleware } from 'redux';
import ReduxThunk from 'redux-thunk';
import reducers from './reducers';

const App = () => {
  const createStoreWithMiddleware = applyMiddleware(ReduxThunk)(createStore);
  return (
    <Provider store={createStoreWithMiddleware(reducers)}>
      <div>Hello world!</div>
    </Provider>
  );
};

export default App;
