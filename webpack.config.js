require('dotenv-extended').load({
  errorOnMissing: true,
});

const { resolve } = require('path');
const webpack = require('webpack');
const BrowserSyncPlugin = require('browser-sync-webpack-plugin');
const ProgressBarPlugin = require('progress-bar-webpack-plugin');
const htmlTemplate = require('html-webpack-template');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const ExtractTextPlugin = require('extract-text-webpack-plugin');
const { BundleAnalyzerPlugin } = require('webpack-bundle-analyzer');
const WatchMissingNodeModulesPlugin = require('react-dev-utils/WatchMissingNodeModulesPlugin');
const { getIfUtils, removeEmpty } = require('webpack-config-utils');
const UglifyJSPlugin = require('uglifyjs-webpack-plugin');

// const CompressionPlugin = require('compression-webpack-plugin');

const CURRENT_IP = require('my-local-ip')();
const externalPath = `http://${CURRENT_IP}:${8080}/`;
const { ifProduction, ifNotProduction, ifDevelopment } = getIfUtils(
  process.env.NODE_ENV
);
const rootNodeModulesPath = `${__dirname}/node_modules`;

module.exports = {
  context: `${__dirname}/src`,
  devtool: ifProduction(
    !!process.env.SOURCE_MAP && 'source-map',
    'cheap-module-eval-source-map'
  ),
  stats: {
    colors: true,
    children: false,
    chunks: false,
    chunkModules: false,
    modules: false,
  },
  devServer: {
    port: process.env.WEBPACK_SERVER_PORT,
    disableHostCheck: true,
    host: '0.0.0.0',
    headers: {
      'Access-Control-Allow-Origin': '*',
    },

    // handle fallback for HTML5 history API
    historyApiFallback: true,

    // show compile errors
    overlay: true,

    // serve public folder
    contentBase: resolve(__dirname, 'public'),
    watchContentBase: true,

    // enable HMR on the server
    hot: true,

    // match the output `publicPath`
    publicPath: ifProduction('/', externalPath),

    // webpack build logs config
    stats: {
      colors: true,
      chunks: false,
    },
  },
  entry: {
    app: ['babel-polyfill', `${__dirname}/src/index.js`],
  },

  resolve: {
    alias: {
      src: `${__dirname}/src`,
      app: `${__dirname}/src/app`,
      styles: `${__dirname}/src/styles`,
      lib: `${__dirname}/lob`,
    },
    modules: ['node_modules', 'shared'],
  },
  output: {
    publicPath: ifDevelopment(externalPath, '/'),
    filename: 'js/[name].js',
    chunkFilename: 'js/chunk.[name].[hash:8].js',
    path: resolve(__dirname, 'public'),
    pathinfo: ifNotProduction(),
  },
  module: {
    rules: [
      {
        test: /\.js$/,
        loader: 'babel-loader',
        exclude: /node_modules/,
        options: {
          cacheDirectory: ifDevelopment(),
        },
      },
      {
        test: /\.(scss)$/,
        loader: ['style-loader', 'css-loader', 'postcss-loader', 'sass-loader'],
      },
      {
        test: /\.(css)$/,
        loader: ['style-loader', 'css-loader'],
      },
      {
        test: /\.(png|jpe?g|gif)(\?.*)?$/,
        loader: 'file-loader',
        options: {
          name: ifProduction('img/[name].[hash:8].[ext]', '[name].[ext]'),
        },
      },
      {
        test: /\.svg(\?v=\d+.\d+.\d+)?$/,
        loader: 'url-loader',
        options: {
          limit: 10000,
          mimetype: 'image/svg+xml',
          name: ifProduction('img/[name].[hash:8].[ext]', '[name].[ext]'),
        },
      },
      {
        test: /\.eot(\?.*)?$/,
        loader: 'url-loader',
        options: {
          limit: 10000,
          mimetype: 'application/vnd.ms-fontobject',
          name: ifProduction('fonts/[name].[hash:8].[ext]', '[name].[ext]'),
        },
      },
      {
        test: /\.otf(\?.*)?$/,
        loader: 'url-loader',
        options: {
          limit: 10000,
          mimetype: 'font/opentype',
          name: ifProduction('fonts/[name].[hash:8].[ext]', '[name].[ext]'),
        },
      },
      {
        test: /\.ttf(\?v=\d+.\d+.\d+)?$/,
        loader: 'url-loader',
        options: {
          limit: 10000,
          mimetype: 'application/octet-stream',
          name: ifProduction('fonts/[name].[hash:8].[ext]', '[name].[ext]'),
        },
      },
      {
        test: /\.woff(\?.*)?$/,
        loader: 'url-loader',
        options: {
          limit: 10000,
          mimetype: 'application/font-woff',
          name: ifProduction('fonts/[name].[hash:8].[ext]', '[name].[ext]'),
        },
      },
      {
        test: /\.woff2(\?.*)?$/,
        loader: 'url-loader',
        options: {
          limit: 10000,
          mimetype: 'application/font-woff2',
          name: ifProduction('fonts/[name].[hash:8].[ext]', '[name].[ext]'),
        },
      },
    ],
  },
  plugins: removeEmpty([
    // define globals
    new webpack.DefinePlugin({
      'process.env': {
        NODE_ENV: ifDevelopment('"development"', '"production"'),
      },
    }),

    new webpack.LoaderOptionsPlugin({
      // css loader config
      minimize: ifProduction(),

      debug: ifNotProduction(),
    }),

    // any required modules inside node_modules are extracted to vendor
    new webpack.optimize.CommonsChunkPlugin({
      name: 'vendor',
      minChunks({ resource }, count) {
        return (
          resource
          && /\.js$/.test(resource)
          && resource.indexOf(rootNodeModulesPath) === 0
        );
      },
    }),

    // extract manifest
    new webpack.optimize.CommonsChunkPlugin({
      name: 'manifest',
      minChunks: Infinity,
    }),

    ifProduction(
      new webpack.optimize.CommonsChunkPlugin({
        async: 'common',
        minChunks(module, count) {
          return count >= 2;
        },
      })
    ),

    // ensures npm install <library> forces a project rebuild
    ifDevelopment(new WatchMissingNodeModulesPlugin(rootNodeModulesPath)),

    // enable HMR globally
    ifDevelopment(new webpack.HotModuleReplacementPlugin()),
    // don't compile if error
    ifDevelopment(new webpack.NoEmitOnErrorsPlugin()),

    // prints more readable module names in the browser console on HMR updates
    ifNotProduction(new webpack.NamedModulesPlugin()),

    ifProduction(
      // minify and optimize the javaScript
      new UglifyJSPlugin({
        uglifyOptions: {
          sourceMap: !!process.env.SOURCE_MAP,
          compress: {
            warnings: false,
          },
          output: {
            comments: false,
          },
        },
      })
    ),

    process.env.BUNDLE_ANALYZER_REPORT
      && ifProduction(new BundleAnalyzerPlugin()),

    new ExtractTextPlugin({
      filename: ifProduction(
        'css/bundle.[name].[contenthash:8].css',
        'bundle.[name].css'
      ),
      disable: ifNotProduction(),
    }),

    new HtmlWebpackPlugin({
      chunksSortMode: 'dependency',
      inject: false,
      filename: `${__dirname}/public/index.html`,
      template: htmlTemplate,
      meta: [
        {
          name: 'description',
          content: 'Blog App React - Redux Course',
        },
        {
          name: 'title',
          content: 'Blog App',
        },
        {
          'http-equiv': 'Content-Type',
          content: 'text/html; charset=utf-8',
        },
      ],
      baseHref: '/',
      bodyHtmlSnippet: `
      <noscript>
          You need to enable JavaScript to run this app.
      </noscript>
      <div id="root"></div>
      `,
      mobile: true,
      lang: 'es-ES',
      title: 'Blog App',
      links: [
        {
          href:
            'https://cdnjs.cloudflare.com/ajax/libs/semantic-ui/2.3.1/semantic.min.css',
          rel: 'stylesheet',
        },
        {
          href: 'https://use.fontawesome.com/releases/v5.1.0/css/all.css',
          rel: 'stylesheet',
          media: 'none',
          onload: "if(media!='all')media='all'",
        },
        {
          href: 'https://cdn.linearicons.com/free/1.0.0/icon-font.min.css',
          rel: 'stylesheet',
          media: 'none',
          onload: "if(media!='all')media='all'",
        },
        {
          href:
            'https://cdnjs.cloudflare.com/ajax/libs/animate.css/3.5.2/animate.min.css',
          rel: 'stylesheet',
          media: 'none',
          onload: "if(media!='all')media='all'",
        },
        {
          href: 'css/styles.css',
          rel: 'stylesheet',
        },
        {
          href: 'css/utils.css',
          rel: 'stylesheet',
        },
        {
          href: 'css/responsive.css',
          rel: 'stylesheet',
        },
      ],
    }),

    process.env.BROWSER_SYNC
      && ifNotProduction(
        new BrowserSyncPlugin(
          {
            open: false,
            port: process.env.BROWSER_SYNC_PORT,
            proxy: externalPath,
          },
          {
            // prevent BrowserSync from reloading the page
            // and let Webpack Dev Server take care of this
            reload: false,
          }
        )
      ),

    new ProgressBarPlugin(),

    /* ifProduction(new CompressionPlugin({
      asset: '[path].gz[query]',
      algorithm: 'gzip',
      test: /\.js$|\.css$|\.html$|\.eot?.+$|\.ttf?.+$|\.woff?.+$|\.svg?.+$/,
      threshold: 10240,
      minRatio: 0.8,
    })), */
  ]),
};
